import hashlib
import json
import threading
import uuid
from datetime import datetime
from os import mkdir, environ, chmod
from os.path import join, exists

import requests
import yaml
from kubernetes import client, config

try:
    from from_registry import get_client, get_pe
    from settings import LOGIN_URL, CWL_URL
except (BaseException, Exception):
    from app.from_registry import get_pe, get_client
    from app.settings import LOGIN_URL, CWL_URL


def init_from_yaml(name_space):
    """
    Get the hostname of the current pod using the kubernetes yaml configuration file of the exec-api

    Args
        namespace (str): the namespace where the execution api is deployed in kubernetes

    Returns
        str: the hostname of the container
    """
    # Rbac authorization for inter container kubectl api calls
    config.load_incluster_config()
    # Init client
    v1 = client.CoreV1Api()
    # Get pods in dare-api namespace
    ret = v1.list_namespaced_pod(name_space)
    # Find ngix-api pod
    for i in ret.items:
        if i.metadata.name == environ['HOSTNAME']:
            return i


def my_pods(username, name_space):
    """
    Function to list all pods related to a specific MPI job

    Args
        | token: user's authentication token
        | name_space: the namespace where the pods run

    Returns
        list: list of the active pods running a job
    """
    a_token = hashlib.sha256(username.encode()).hexdigest()[:6]
    # Init client
    v1 = client.CoreV1Api()
    # Get pods in dare-api namespace
    ret = v1.list_namespaced_pod(name_space)
    pods = []
    # For each pod
    for i in ret.items:
        # Check if mpi cluster has been initialized
        if a_token in i.metadata.name:
            obj = {'name': i.metadata.name, 'status': i.status.phase}
            pods.append(obj)
    return pods


class JobPreparator:
    # Constants
    RUNS = "runs"
    UPLOADS = "uploads"
    DEBUG = "debug"
    OUTPUT = "output"
    CWL = "cwl"
    group = "kubeflow.org"
    version = "v1alpha1"
    plural = "mpijobs"

    run_id = None
    run_dir = None
    pod_name = None
    name_space = None
    image = None
    logger = None
    delegation_token = None
    nodes = 1
    workflow_name = None
    job_body = None
    user_runs_folder = None
    issuer = None
    user_id = None
    username = None
    kind = None
    kinds = ["run", "kill"]
    killed_job = False

    def __init__(self, d4p_settings, namespace, user_id, username, mountpath, pod_prefix, pod_suffix, token, issuer,
                 nodes, logger, kind="run", run_dir=None):
        self.logger = logger
        self.kind = kind
        self.d4p_settings = d4p_settings
        self.name_space = namespace
        self._init_pod_name(username, pod_prefix, pod_suffix)
        self.user_id = user_id
        self.username = username
        self.user_runs_folder = join(mountpath, username, self.RUNS)
        if kind == "run":
            self.run_id = uuid.uuid4().hex
            self._init_run_dir()
            self.nodes = nodes if nodes else self.nodes
            self.issuer = issuer

            try:
                self.delegation_token = self.get_delegation_token(token)
            except (BaseException, Exception) as e:
                self.logger.error(e)
                self.delegation_token = token
        else:
            self.run_dir = run_dir
            self.delete_job()

    def create_job(self):
        # Rbac authorization for inter container kubectl api calls
        config.load_incluster_config()
        # create an instance of the API class
        api_instance = client.CustomObjectsApi(client.ApiClient())

        try:
            api_response = api_instance.create_namespaced_custom_object(self.group, self.version, self.name_space,
                                                                        self.plural, self.job_body)
            self.logger.info("API response for creating container: {}".format(api_response))
            # Monitor cluster status
            _th = threading.Thread(target=self.monitor_job)
            _th.start()
            return json.dumps({"run_dir": self.run_dir, "run_id": self.run_id, "job_name": self.pod_name}), 200
        except Exception as e:
            msg = "An error occurred while trying to spawn the container: {}".format(e)
            self.logger.error(msg)
            return msg, 500

    def monitor_job(self):
        config.load_incluster_config()
        job_finished = False
        while True:
            # Init client
            v1 = client.BatchV1Api()
            # Get pods in dare-api namespace
            ret = v1.list_namespaced_job(self.name_space)
            # For each pod
            for job in ret.items:
                # Check if mpi cluster has been initialized
                if self.pod_name + "-launcher" in job.metadata.name:
                    # If mpijob is finished, then delete cluster
                    if (job.status.failed and job.status.failed > 0) or \
                            (job.status.succeeded and job.status.succeeded > 0):
                        self.logger.info("Job finished!")
                        self._collect_execution_logs(job)
                        self.logger.info("Logs are collected")
                        self._delete_mpi_job()
                        self.logger.info("MPIJob is deleted")
                        job_finished = True
                        break
            if job_finished:
                break

    def delete_job(self):
        config.load_incluster_config()
        # Init client
        v1 = client.BatchV1Api()
        # Get pods in dare-api namespace
        ret = v1.list_namespaced_job(self.name_space)
        # For each pod
        for job in ret.items:
            # Check if mpi cluster has been initialized
            if self.pod_name + "-launcher" in job.metadata.name:
                self.logger.info("Found launcher container for job: {}".format(self.pod_name))
                self._collect_execution_logs(job)
                # Delete mpi cluster
                self._delete_mpi_job()
                self.killed_job = True
                break
            elif self.pod_name in job.metadata.name:
                # Delete mpi cluster
                self._delete_mpi_job()
                self.killed_job = True
                break

    def _collect_execution_logs(self, job):
        self.logger.info("Found launcher container for job: {}".format(self.pod_name))
        _v1 = client.CoreV1Api()
        ret = _v1.list_namespaced_pod(self.name_space)
        for i in ret.items:
            if job.metadata.name in i.metadata.name:
                logs = _v1.read_namespaced_pod_log(i.metadata.name, self.name_space)
                self.logger.info("-----------Logs for execution container {}-launcher ---------".format(self.pod_name))
                self.logger.info(logs)
                self.logger.info("--------------------------------------------------------------")
                output_run_dir = join(self.user_runs_folder, self.run_dir)
                with open(join(output_run_dir, self.OUTPUT, "logs.txt"), "w") as f:
                    f.write(logs)
                break

    def _delete_mpi_job(self):
        api_instance = client.CustomObjectsApi(client.ApiClient())
        # MPIJOB specifics
        name = self.pod_name
        body = client.V1DeleteOptions()
        # Delete mpi cluster
        try:
            api_instance.delete_namespaced_custom_object(self.group, self.version, self.name_space, self.plural,
                                                         name=name, body=body)
        except(ValueError, Exception) as e:
            self.logger.error("Exception while deleting job with name {}".format(self.pod_name))
            self.logger.error(e)

    def _init_run_dir(self):
        self.run_dir = self.username + "_" + datetime.now().strftime("%Y%m%d%H%M%S") + "_" + str(self.run_id)
        if not exists(join(self.user_runs_folder, self.run_dir)):
            mkdir(join(self.user_runs_folder, self.run_dir))
            mkdir(join(self.user_runs_folder, self.run_dir, self.OUTPUT))
            chmod(join(self.user_runs_folder, self.run_dir), 0o777)
            chmod(join(self.user_runs_folder, self.run_dir, self.OUTPUT), 0o777)

    def _init_pod_name(self, username, pod_prefix, pod_suffix):
        a_token = hashlib.sha256(username.encode()).hexdigest()[:10]
        self.pod_name = pod_prefix + '-' + a_token + pod_suffix if pod_suffix else pod_prefix + "-" + a_token

    def get_delegation_token(self, token):
        try:
            url = "{}/delegation-token".format(LOGIN_URL)
            response = requests.post(url, data=json.dumps({"access_token": token, "issuer": self.issuer}))
            if response.status_code != 200:
                return token
            else:
                delegation_token_response = json.loads(response.text)
                return delegation_token_response["access_token"]
        except (ConnectionError, Exception) as e:
            self.logger.error("An error occurred while issuing a delegation token: {}".format(e))
            return token

    def get_image(self, image_name):
        raise NotImplementedError

    def get_workflow(self, token):
        raise NotImplementedError

    def create_job_body(self, token, input_data):
        raise NotImplementedError


class CwlJobPreparator(JobPreparator):
    workflow_version = None
    spec_name = None

    def __init__(self, d4p_settings, namespace, user_id, username, mountpath, pod_prefix, pod_suffix, token, issuer,
                 nodes, workflow_name, workflow_version, input_data, logger, save_workflow=None):
        super(CwlJobPreparator, self).__init__(d4p_settings, namespace, user_id, username, mountpath, pod_prefix,
                                               pod_suffix, token, issuer, nodes, logger)
        self.workflow_name = workflow_name
        self.workflow_version = workflow_version
        self.save_workflow = save_workflow
        logger.debug("Creating cwl job with name {} and version {}".format(self.workflow_name, self.workflow_version))
        self.found_workflow = self.get_workflow(token)
        self.create_job_body(token, input_data)

    def get_workflow(self, token):
        """
        Example response:
        ================
        {
        "id": 2,
        "name": "demo_workflow.cwl",
        "version": "v1.0",
        "workflow": ""
        "spec_name": "spec.yaml",
        "spec": "",
        "user_identifier": "sthemeli#dare#dare",
        "docker_env": {
            "id": 3,
            "creation_date": "2020-04-10T07:00:55.674717Z",
            "created_by": "sthemeli#dare#dare",
            "modification_date": "2020-04-10T09:15:15.009044Z",
            "modified_by": "sthemeli#dare#dare",
            "name": "test",
            "tag": "v1.0",
            "url": "https://gitlab.com/project-dare/dare-platform/test:v1.0",
            "user_identifier": "sthemeli#dare#dare",
            "dockerfile": ""
        },
        "creation_date": "2020-04-10T09:59:32.834978Z",
        "created_by": "sthemeli#dare#dare",
        "modification_date": "2020-04-10T09:59:32.834982Z",
        "modified_by": "sthemeli#dare#dare",
        "workflow_parts": [
            {
                "id": 3,
                "name": "arguments.cwl",
                "version": "v1.1",
                "spec_name": "arguments.yaml",
                "workflow": {
                    "id": 2,
                    "creation_date": "2020-04-10T09:59:32.834978Z",
                    "created_by": "sthemeli#dare#dare",
                    "modification_date": "2020-04-10T09:59:32.834982Z",
                    "modified_by": "sthemeli#dare#dare",
                    "name": "demo_workflow.cwl",
                    "version": "v1.0",
                    "workflow": ""
                    "spec_name": "spec.yaml",
                    "spec": ""
                    "user_identifier": "sthemeli#dare#dare",
                    "docker_env": 3
                },
                "script": ""
                "spec": "",
                "creation_date": "2020-04-10T10:10:36.376026Z",
                "created_by": "sthemeli#dare#dare",
                "modification_date": "2020-04-10T10:58:26.672318Z",
                "modified_by": "sthemeli#dare#dare"
            },
            {
                "id": 5,
                "name": "tar_param.cwl",
                "version": "v1.0",
                "spec_name": "tar_param.yaml",
                "workflow": {
                    "id": 2,
                    "creation_date": "2020-04-10T09:59:32.834978Z",
                    "created_by": "sthemeli#dare#dare",
                    "modification_date": "2020-04-10T09:59:32.834982Z",
                    "modified_by": "sthemeli#dare#dare",
                    "name": "demo_workflow.cwl",
                    "version": "v1.0",
                    "workflow": ""
                    "spec_name": "spec.yaml",
                    "spec": ""
                    "user_identifier": "sthemeli#dare#dare",
                    "docker_env": 3
                },
                "script": ""
                "spec": "",
                "creation_date": "2020-04-10T10:41:44.792055Z",
                "created_by": "sthemeli#dare#dare",
                "modification_date": "2020-04-10T10:41:44.792059Z",
                "modified_by": "sthemeli#dare#dare"
            }
        ]
    }
        """
        self.logger.debug("Trying to retrieve workflow from registry")
        url = CWL_URL + "/workflows/bynameversion"
        data = {
            "workflow_name": self.workflow_name,
            "workflow_version": self.workflow_version,
            "access_token": token
        }
        response = requests.get(url, params=data)
        if response.status_code == 200:
            self.logger.debug("Workflow found!")
            response = json.loads(response.text)
            image_name = response["docker_env"]["url"]
            self.get_image(image_name)
            self.logger.debug("Using image : {}".format(self.image))
            self.spec_name = response["spec_name"]
            if self.save_workflow:
                self._save_workflow(response, token)
                self.logger.debug("Workflow saved in the execution directory!")
            return True
        else:
            self.logger.error("Workflow Registry returned status code: {} and response {}".format(response.status_code,
                                                                                                  response.text))
            return False

    def create_job_body(self, token, input_data):
        self.logger.debug("Wokrlfow input data: {}".format(input_data))
        self.job_body = {
            "apiVersion": "kubeflow.org/v1alpha1",
            "kind": "MPIJob",
            "metadata": {
                "name": self.pod_name,
                "namespace": self.name_space
            },
            "spec": {
                "replicas": int(self.nodes),
                "template": {
                    "spec": {
                        "containers": [
                            {
                                "image": self.image,
                                "imagePullPolicy": "Always",
                                "name": self.pod_name,
                                "env": [
                                    {
                                        "name": "ACCESS_TOKEN",
                                        "value": token
                                    },
                                    {
                                        "name": "DELEGATION_TOKEN",
                                        "value": self.delegation_token
                                    },
                                    {
                                        "name": "ISSUER",
                                        "value": self.issuer
                                    },
                                    {
                                        "name": "RUN_ID",
                                        "value": self.run_id
                                    },
                                    {
                                        "name": "RUN_DIR",
                                        "value": self.run_dir
                                    },
                                    {
                                        "name": "USERNAME",
                                        "value": self.username
                                    },
                                    {
                                        "name": "USER_ID",
                                        "value": self.user_id
                                    },
                                    {
                                        "name": "WORKFLOW_NAME",
                                        "value": self.workflow_name
                                    },
                                    {
                                        "name": "WORKFLOW_VERSION",
                                        "value": self.workflow_version
                                    },
                                    {
                                        "name": "SPEC_NAME",
                                        "value": self.spec_name
                                    },
                                    {
                                        "name": "INPUT_DATA",
                                        "value": input_data
                                    }
                                ],
                                "volumeMounts": [
                                    {
                                        "name": self.d4p_settings["volname"],
                                        "mountPath": self.d4p_settings["mountpath"]
                                    }
                                ]
                            }
                        ],
                        "volumes": [
                            {
                                "name": self.d4p_settings["volname"],
                                "flexVolume": {
                                    "driver": "ceph.rook.io/rook",
                                    "fsType": "ceph",
                                    "options": {
                                        "fsName": self.d4p_settings["fsname"],
                                        "clusterNamespace": "rook-ceph",
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }

        self.logger.debug("Specification body for CWL: {}".format(json.dumps(self.job_body)))

    def get_image(self, image_name):
        self.image = image_name

    def _save_workflow(self, response, token):
        run_dir_path = join(self.user_runs_folder, self.run_dir)
        with open(join(run_dir_path, self.workflow_name), "w") as f:
            workflow = response["workflow"]
            f.write(workflow)
        with open(join(run_dir_path, self.spec_name), "w") as f:
            spec = response["spec"]
            f.write(spec)
        with open(join(run_dir_path, self.spec_name), "r") as f:
            spec = yaml.safe_load(f.read())
        with open(join(run_dir_path, self.spec_name), "w") as f:
            spec["access_token"] = token
            spec["delegation_token"] = self.delegation_token
            spec["issuer"] = self.issuer
            spec["user_id"] = self.user_id
            spec["run_id"] = self.run_id
            spec["prov_config"] = "inline"
            yaml.safe_dump(spec, f)

        workflow_parts = response.get("workflow_parts", None)

        if workflow_parts:
            for workflow_part in workflow_parts:
                with open(join(run_dir_path, workflow_part["name"]), "w") as f:
                    workflow_p = workflow_part["script"]
                    f.write(workflow_p)
                if "spec_name" in workflow_part.keys() and workflow_part["spec_name"]:
                    with open(join(run_dir_path, workflow_part["spec_name"]), "w") as f:
                        workflow_part_spec = workflow_part["spec_name"]
                        f.write(workflow_part_spec)


class D4pJobPreparator(JobPreparator):
    workflow_id = None
    workspace_id = None
    package = "main"
    d4p_args = None
    found_workflow = False
    reqs = None

    def __init__(self, d4p_settings, namespace, user_id, username, mountpath, pod_prefix, pod_suffix, token, issuer,
                 nodes, workflow_name, workflow_id, workspace_id, logger, d4p_args, reqs, image_name=None,
                 package_name=None):
        super(D4pJobPreparator, self).__init__(d4p_settings, namespace, user_id, username, mountpath, pod_prefix,
                                               pod_suffix, token, issuer, nodes, logger)
        self.workflow_name = workflow_name
        self.workflow_id = workflow_id
        self.workspace_id = workspace_id
        self.package = package_name if package_name else self.package
        self.d4p_args = d4p_args
        self.reqs = reqs
        self.found_workflow = self.get_workflow(token)
        self.get_image(image_name)
        self.create_job_body(token, self.d4p_args)

    def get_image(self, image_name):
        image_name = image_name if image_name else "latest"
        self.logger.debug("Image tag to be used: {}".format(image_name))
        self.image = "registry.gitlab.com/project-dare/dare-platform/exec-context-d4p:{}".format(image_name)

    def get_workflow(self, token):
        """
        Access the D4P Registry to retrieve the code of a specific PE implementation

        Args
            data (dict): request data provided by the user in order to be used to find the PE Implementation
            in the D4p Information Registry

        Returns
            str: the code found in the registry
        """
        try:
            # Authenticate user / GET auth token
            verce_client = get_client()
            # Get ProcessingElement Implementation code
            code = get_pe(self.workflow_id, self.workspace_id, self.package, self.workflow_name, token, verce_client)
            run_dir_path = join(self.user_runs_folder, self.run_dir)

            with open(join(run_dir_path, "code.py"), 'w') as f:
                f.write(code)
            spec = self._mpi_input(join(run_dir_path, "code.py"), self.d4p_args)
            spec["issuer"] = self.issuer
            spec["userid"] = '@'.join((self.user_id, self.issuer))
            spec["access_token"] = token
            spec["provenance-bearer-token"] = self.delegation_token
            spec["run_id"] = self.run_id
            spec["provenance-runid"] = self.run_id
            spec["provenance-config"] = "inline"
            spec["provenance-repository-url"] = "http://prov-queue:5000/workflowexecutions/insert"
            mpi_spec = yaml.dump(spec)

            with open(join(run_dir_path, 'spec.yaml'), 'w') as f:
                f.write(mpi_spec)
            return True
        except(BaseException, Exception) as e:
            self.logger.error("An exception occurred while retrieving workflow {}! {}".format(self.workflow_name, e))

    def create_job_body(self, token, input_data):
        self.job_body = {"apiVersion": "kubeflow.org/v1alpha1",
                         "kind": "MPIJob",
                         "metadata": {
                             "name": self.pod_name,
                             "namespace": self.name_space
                         },
                         "spec": {
                             "replicas": int(self.nodes),
                             "template": {
                                 "spec": {
                                     "containers": [
                                         {
                                             "image": self.image,
                                             "imagePullPolicy": "Always",
                                             "name": self.pod_name,
                                             "env": [
                                                 {
                                                     "name": "D4P_RUN_DATA",
                                                     "value": input_data
                                                 },
                                                 {
                                                     "name": "USER_REQS",
                                                     "value": self.reqs
                                                 },
                                                 {
                                                     "name": "RUN_ID",
                                                     "value": self.run_id
                                                 },
                                                 {
                                                     "name": "RUN_DIR",
                                                     "value": self.run_dir
                                                 },
                                                 {
                                                     "name": "USERNAME",
                                                     "value": self.username
                                                 },
                                                 {
                                                     "name": "USER_ID",
                                                     "value": self.user_id
                                                 },
                                                 {
                                                     "name": "TOKEN",
                                                     "value": token
                                                 },
                                                 {
                                                     "name": "DELEGATION_TOKEN",
                                                     "value": self.delegation_token
                                                 },
                                                 {
                                                     "name": "ISSUER",
                                                     "value": self.issuer
                                                 }
                                             ],
                                             "volumeMounts": [
                                                 {
                                                     "name": self.d4p_settings["volname"],
                                                     "mountPath": self.d4p_settings["mountpath"]
                                                 }
                                             ]
                                         }
                                     ],
                                     "volumes": [
                                         {
                                             "name": self.d4p_settings["volname"],
                                             "flexVolume": {
                                                 "driver": "ceph.rook.io/rook",
                                                 "fsType": "ceph",
                                                 "options": {
                                                     "fsName": self.d4p_settings["fsname"],
                                                     "clusterNamespace": "rook-ceph",
                                                 }
                                             }
                                         }
                                     ]
                                 }
                             }
                         }
                         }
        self.logger.debug("Specification body for d4p: {}".format(json.dumps(self.job_body)))

    @staticmethod
    def _mpi_input(workflow, kw):
        """
        Function used in the d4p execution context. Generates a yaml file for the CWL configuration in order to
        execute a dispel4py workflow

        Args:
            | workflow (str): path of the workflow code in the execution container
            | kw (dict): additional parameters in dict format

        Returns
            dict: yaml specification for the CWL execution
        """
        kw = json.loads(kw)
        output_data = {
            'd4py': 'dispel4py',
            'target': 'mpi',
            'workflow': workflow,
        }
        for k in kw:
            output_data[k] = kw.get(k)
            if isinstance(output_data[k], dict):
                output_data[k] = json.dumps(output_data[k])
        return output_data
