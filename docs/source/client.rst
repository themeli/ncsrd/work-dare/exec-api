################################
Client-side Helper Functions
################################

.. toctree::
        dare_manager
        registries_manager
        exec_api_manager

DARE platform provides helper functions in order to interact with its RESTful APIs. In this section, we provide technical
documentation on the client-side DARE library. This library included client-side functions for the two registries, i.e.
the Dispel4py and the CWL workflow registries and the Execution API. DareManger class wraps all the functionality of those
two libraries and exposes it to the users.