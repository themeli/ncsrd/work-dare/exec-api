from os.path import join, exists
from os import getcwd, mkdir

import requests


def download_docker_files():
    docker_folder = join(getcwd(), "docker_files")
    if not exists(docker_folder):
        mkdir(docker_folder)

    dockerfile = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/Dockerfile")
    with open(join(docker_folder, "Dockerfile"), "w") as f:
        f.write(dockerfile.text)

    entrypoint = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/entrypoint.sh")
    with open(join(docker_folder, "entrypoint.sh"), "w") as f:
        f.write(entrypoint.text)

    input_files = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/input_files.txt")
    with open(join(docker_folder, "input_files.txt"), "w") as f:
        f.write(input_files.text)

    input_files2 = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/input_files2.txt")
    with open(join(docker_folder, "input_files2.txt"), "w") as f:
        f.write(input_files2.text)

    input_files3 = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/input_files3.txt")
    with open(join(docker_folder, "input_files3.txt"), "w") as f:
        f.write(input_files3.text)

    cyclone_config = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/cyclone_config_CMIP6.json")
    with open(join(docker_folder, "cyclone_config_CMIP6.json"), "w") as f:
        f.write(cyclone_config.text)

    config_cmip6 = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-cyclone/cwl/config_cmip6.txt")
    with open(join(docker_folder, "config_cmip6.txt"), "w") as f:
        f.write(config_cmip6.text)


def download_cwl_files():
    cwl_folder = join(getcwd(), "cwl_files")
    if not exists(cwl_folder):
        mkdir(cwl_folder)

    # CWL Workflow class
    workflow = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/tracking_master.cwl")
    with open(join(cwl_folder, "tracking_master.cwl"), "w") as f:
        f.write(workflow.text)

    spec = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/tracking_master.yml")
    with open(join(cwl_folder, "spec.yaml"), "w") as f:
        f.write(spec.text)

    # Env preparation step
    env_prep_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/env_preparation.cwl")
    with open(join(cwl_folder, "env_preparation.cwl"), "w") as f:
        f.write(env_prep_cwl.text)
    env_prep_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/env_preparation.sh")
    with open(join(cwl_folder, "env_preparation.sh"), "w") as f:
        f.write(env_prep_sh.text)

    # Process files step
    process_files_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/processfiles.cwl")
    with open(join(cwl_folder, "processfiles.cwl"), "w") as f:
        f.write(process_files_cwl.text)
    process_files_py = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/processfiles.py")
    with open(join(cwl_folder, "processfiles.py"), "w") as f:
        f.write(process_files_py.text)
    process_files_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/processfiles.sh")
    with open(join(cwl_folder, "processfiles.sh"), "w") as f:
        f.write(process_files_sh.text)

    # Transfer files step
    transfer_files_cwl = requests.get(
        "https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/transferfiles.cwl")
    with open(join(cwl_folder, "transferfiles.cwl"), "w") as f:
        f.write(transfer_files_cwl.text)
    transfer_files_py = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/transferfiles.py")
    with open(join(cwl_folder, "transferfiles.py"), "w") as f:
        f.write(transfer_files_py.text)
    transfer_files_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/transferfiles.sh")
    with open(join(cwl_folder, "transferfiles.sh"), "w") as f:
        f.write(transfer_files_sh.text)

    # extractnc step
    extractnc_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/extractnc.cwl")
    with open(join(cwl_folder, "extractnc.cwl"), "w") as f:
        f.write(extractnc_cwl.text)
    extractnc_py = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/extractnc.py")
    with open(join(cwl_folder, "extractnc.py"), "w") as f:
        f.write(extractnc_py.text)
    extractnc_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/extractnc.sh")
    with open(join(cwl_folder, "extractnc.sh"), "w") as f:
        f.write(extractnc_sh.text)

    # make_tracks step
    make_tracks_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/make_tracks.cwl")
    with open(join(cwl_folder, "make_tracks.cwl"), "w") as f:
        f.write(make_tracks_cwl.text)
    make_tracks_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/make_tracks.sh")
    with open(join(cwl_folder, "make_tracks.sh"), "w") as f:
        f.write(make_tracks_sh.text)

    # xml2ascii step
    xml2ascii_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/xml2ascii.cwl")
    with open(join(cwl_folder, "xml2ascii.cwl"), "w") as f:
        f.write(xml2ascii_cwl.text)
    xml2ascii_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/xml2ascii.sh")
    with open(join(cwl_folder, "xml2ascii.sh"), "w") as f:
        f.write(xml2ascii_sh.text)

    # postprocess step
    postprocess_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/postprocess.cwl")
    with open(join(cwl_folder, "postprocess.cwl"), "w") as f:
        f.write(postprocess_cwl.text)
    postprocess_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/postprocess.sh")
    with open(join(cwl_folder, "postprocess.sh"), "w") as f:
        f.write(postprocess_sh.text)

    plots_cwl = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/plots.cwl")
    with open(join(cwl_folder, "plots.cwl"), "w") as f:
        f.write(plots_cwl.text)
    plots_py = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/plots.py")
    with open(join(cwl_folder, "plots.py"), "w") as f:
        f.write(plots_py.text)
    plots_sh = requests.get("https://gitlab.com/project-dare/wp7_cyclone-tracking/-/raw/cwl/plots.sh")
    with open(join(cwl_folder, "plots.sh"), "w") as f:
        f.write(plots_sh.text)